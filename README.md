# pacolog
pacolog  (**pa**cman **co**mmit **log**) lists recent commits for Arch Linux packages, querying packages from both [official repositories](https://wiki.archlinux.org/index.php/Official_repositories) and the [AUR](http://aur.archlinux.org/).

## Usage
* Specify the package as the argument, e.g. `pacolog vim`
* Optionally, specify the number of recent commits to view with `-l NUM`

Query the main repositories
```
$ pacolog -l 3 vim
upgpkg: 9.1.0954-1: update to 9.1.0954 . bb211780
T.J. Townsend authored Dec 22, 2024
9.1.0954-1

upgpkg: 9.1.0909-1: update to 9.1.0909 . 93cb8930
T.J. Townsend authored Dec 06, 2024
9.1.0909-1

upgpkg: 9.1.0866-2: python 3.13 rebuild . 778f2e5a
Antonio Rojas authored Nov 16, 2024
9.1.0866-2
```

Or the AUR
```
$ pacolog -l 3 yay
   Age       Commit message                   Author
2024-09-19 12.4.2                         Jo
2024-09-17 v12.4.1                        Jo
2024-09-14 update pkgver                  jguer
```

## Dependencies
* `w3m`
* `sed`, `gawk`, `grep` (all in `base-devel` for Arch Linux)
* Network access is required, since pacman does not store commit logs locally

## Acknowledgements
* Thanks to [dolik.rce](https://bbs.archlinux.org/profile.php?id=48434) for useful hints in the Arch Linux [forums](https://bbs.archlinux.org/viewtopic.php?pid=1591586)
